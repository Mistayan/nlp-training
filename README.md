# Qualité des données

## Description

Ce projet a pour but de mettre en place une solution permettant d'analyser la qualité des données d'une entreprise, et
de proposer des solutions pour améliorer cette qualité.

## Contexte

Ce projet a été réalisé dans le cadre d'un exercice de cours, dans le cadre du Master 1 EISI à l'école EPSI.

## Requirements

`venv`

```shell
python -m venv venv
```

`requirements.txt`

```shell
./venv/Scripts/pip install -r requirements.txt
./venv/lib/pip install -r requirements.txt
```

## Vue d'esemble du projet:
```mermaid
graph LR
Main[Main]
Collect[Collect data]
Clean[clean data]
Sort[Preprocess]
Exploit[Re-Train]

subgraph "main"
IHM
Predict
Main -- starts --> IHM
end

subgraph "Database"
BDD[BDD]
end

IHM -- change cat --> BDD
IHM -- update data and model --> BDD
IHM -- selectionner mail --> Predict -- cat --> IHM



subgraph "cron"
Sort -- cron --> Exploit
Exploit -- save_pickle --> BDD
Clean -- cron --> Sort
Collect -- cron --> Clean
OutlookConnector -- GET --> Collect
end

BDD -- GET --> Collect


```
## Méthodologie de collecte et de tri des données

J'ai décidé d'orienter mes recherches autours du NLP.

Afin de collecter assez de données pour pouvoir entrainer l'algorithme, j'ai décidé de collecter des mails de
type `commerciaux` et leurs `catégories` assignées (opération qui a été effectuée par une équipe dédiée, nous
garantissant une certaine qualité de la donnée extraite),

Pour cela, j'ai utilisé la librairie `pywin32` de python qui permet de se connecter au client Outlook et de récupérer
les mails.

- `src/collect/` : scripts permettant de collecter les données [WIP]

<details>
<summary>Collecte des données</summary>

Avant d'exécuter un script, créez un environnement virtuel [venv](venv). et installez les
dépendances [requirements.txt](requirements.txt)

```shell
```shell
./venv/Scripts/python.exe src/collect/OutlookConnector.py
```

</details>

<details>
<summary>Tri et Nettoyage des données</summary>
Je filtre à la source les mails ne possédant pas de catégories, sur une plage de temps
définie. [plage de temps ou l'on est sûr que les mails ont été catégorisés par les équipes]

- [non commit / non démontré]
  Les données sont stockées dans un fichier csv, généré par l'étape précédente.
  Afin que le projet puisse être testé, il existe des exemples de test

</details>

<details>
<summary>Preprocess des données</summary>

J'ai ensuite utilisé la librairie `nltk` && `spacy` pour catégoriser les contenus des mails et les transformer en
vecteurs de mots, par rapport aux catégories préalablement assignées.
`sklearn` intégré au nlp, afin d'entrainer un modèle de classification.

- `src/MLP` : scripts permettant de créer un modèle à partir des données
  Au chargement du fichier emails.csv, j'affiche les différents éléments sur les quels l'apprentissage va se baser.
  Il est **important de noter que des données très déséquilibrées poseront problème lors de l'entrainement** du modèle.

Après avoir chargé les données, je les transforme en vecteurs de mots, puis je les sépare en deux groupes : un groupe
d'entrainement et un groupe de test.
Je charge ensuite un modèle de classification équipé de multiples algorithms pré-implémentés (tokenizer, parser, lemmatizer ...),
J'ajoute un catégoriseur en fin de flux, prenant en entrée le resultat du dernier algorithme exécuté par le modèle et je l'entraine avec les données d'entrainement.
Je teste ensuite le modèle avec les données de test, et j'affiche les résultats obtenus.
Le script `src/MLP/make_model.py` permet de créer un modèle de classification à partir des données.

```shell
cp src/MLP/fr_commercial_model/ src/categorize/fr_commercial_model/
./venv/Scripts/python.exe src/MLP/make_model.py
```

Il est possible de modifier les paramètres du modèle, et de le sauvegarder pour une utilisation/seconde phase
d'entrainement ultérieure.
</details>

<details>
<summary>Prédiction</summary>
Le modèle est sauvegardé dans le dossier `src/MLP/fr_commercial_model/`
Afin de pouvoir utiliser le modèle, il faut le charger dans le script `src/MLP/MLP_predict.py` et lancer le script.

```shell
cp -r src/MLP/fr_commercial_model/ src/categorize/fr_commercial_model
```

```shell
./venv/Scripts/python.exe src/categorize/categorize_spacy.py "texte à catégoriser"
```

</details>

## Améliorations

Afin de permettre au employés de rectifier certains des résultats obtenus, je compte utiliser la librairie TKinter, afin
de créer une interface graphique permettant de visualiser les résultats obtenus, et de les modifier si besoin.

<details>
<summary>Interface graphique</summary>
L'interface graphique est accessible via le script `src/main.py`

```shell
./venv/Scripts/python.exe src/main.py
```
</details>

## Sources

https://pypi.org/project/langdetect/
https://spacy.io/api/tokenizer
https://spacy.io/models/fr
https://github.com/UniversalDependencies/UD_French-Sequoia/blob/master/README.md
https://prodi.gy/buy
https://pypi.org/project/pywin32/
https://stackoverflow.com/questions/75643277/how-can-i-solve-the-error-the-stop-words-parameter-of-tfidfvectorizer-must-be
