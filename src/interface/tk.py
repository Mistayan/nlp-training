import json
from tkinter import Listbox, END, Button, Label, Tk, ACTIVE, LEFT, Frame, Menu, Text, DISABLED

from src.categorize.categorize_spacy import Categorizer
from src.collect.get_data_from_files import get_csv_data
from src.conf import project_file, LOGGING_LEVEL
from src.utils import Logger


class MyTkApp:
    def __init__(self, file=None):
        self.__logger = Logger(name=__name__, level=LOGGING_LEVEL, log_file='log.txt')
        self.__listbox = None
        self.__current_item = None
        # self.__database = Database()
        self.__categorizer = Categorizer()
        self.root = Tk()
        self.root.title("My Tk App")
        self.root.geometry("800x600")
        self.root.resizable(False, False)
        self.root.config(bg="white")
        self.__categories = self.__get_categories()
        self.__init_page()
        self.__data_file = project_file(file if file else "emails.csv", "data")
        self.actualiser_donnees()
        self.__init_right_panel_text()
        self.__init_listbox()

    # Fonction pour actualiser les données
    def actualiser_donnees(self):
        # self.__logger.info("actualiser_donnees  ... from file: " + self.__data_file)
        print("actualiser_donnees  ... from file: " + self.__data_file)
        self.__data = get_csv_data(self.__data_file)


    def __get_categories(self):
        with open("../data/cat_token_dict.json", "r") as f:
            categories = list(json.load(fp=f).keys())
            categories.append("OK")
            print(categories)
        return categories

    def __init_page(self):
        self.__init_menu()
        self.__init_frame()
        self.__init_footer()

    def run(self):
        self.root.mainloop()


    def __init_menu(self):
        # Création du menu
        menu = Menu(self.root)
        self.root.config(menu=menu)

        # Ajout d'options au menu
        menu_actions = Menu(menu)
        menu.add_cascade(label="Actions", menu=menu_actions)
        menu_actions.add_command(label="Actualiser", command=self.actualiser_donnees)
        menu_actions.add_command(label="Trier", command=self.__categories_button_actions)
        menu_actions.add_separator()
        menu_actions.add_command(label="Quitter", command=self.__exit)

        ...

    def __init_frame(self):
        self.frame = Frame(self.root)
        self.frame.grid(row=5, column=0)
        self.__init_frame_left()
        self.__init_frame_right()
        self.root.pack_slaves()

    def __init_frame_left(self):
        self.frame_left = Frame(self.frame)
        self.frame_left.grid(row=0, column=0)

    def __init_frame_right(self):
        self.frame_right = Frame(self.frame)
        self.frame_right.grid(row=0, column=2)

    def __init_footer(self):
        self.footer = Frame(self.root)
        self.footer.grid(row=1, column=0)
        # add categorisation buttons
        self.__init_footer_categorisation()

    def __init_footer_categorisation(self):
        """ add categorisation buttons """
        self.footer_categorisation = Frame(self.footer)
        self.footer_categorisation.grid(row=2, column=3)
        for cat in self.__categories:
            self.__init_footer_categorisation_buttons(cat)

    def __init_footer_categorisation_buttons(self, cat):
        """ add each categorisation buttons """
        button = Button(self.footer_categorisation, text=cat, command=lambda: self.__categories_button_actions(cat))
        button.pack(side=LEFT)

    def __categories_button_actions(self, cat):
        """ categorise the text and exit current item"""
        """TODO: add categorisation function here"""
        print(cat)
        self.__current_item = self.__listbox.get(ACTIVE)

    def __exit(self):
        """ exit the app """
        self.root.destroy()

    def __about(self):
        """ show about message """
        print("about")
        popup = Tk()
        popup.wm_title("About")
        label = Label(popup, text="This is a simple app to categorize emails")
        label.pack(side="top", fill="x", pady=10)
        B1 = Button(popup, text="Okay", command=popup.destroy)
        B1.pack()
        popup.mainloop()

    def __init_right_panel_text(self):
        self.__text = Text(self.frame_right, height=20, width=50)
        self.__text.grid(row=0, column=0)
        if self.__current_item:
            self.__text.insert(END, self.__current_item)
            self.__text.insert(END, "\n"*2)
            self.__text.insert(END, "Categorie prédite :")
            self.__text.insert(END, self.__categorizer.detect(self.__current_item))
        else:
            self.__text.insert(END, "Bienvenue dans l'application de catégorisation de mails.")
            self.__text.insert(END, "Sélectionnez un mail dans la liste à gauche pour afficher son contenu. et cliquez sur une catégorie pour le catégoriser.")
        self.__text.config(state=DISABLED)

    def __onselect(self, evt):
        # Note here that Tkinter passes an event object to onselect()
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        self.__text.delete(1.0, END)
        self.__text.insert(END, value)
        self.__current_item = value

        # load the email on the right screen
        # self.__text.insert(END, self.__database.get(index))
        self.__init_right_panel_text()

    def __init_listbox(self):
        """ From loaded dataset, create a listbox on the left of the window """
        self.__listbox = Listbox(self.frame_left, width=50, height=20)
        self.__listbox.grid(row=0, column=0)
        self.__listbox.bind('<<ListboxSelect>>', self.__onselect)
        for item in self.__data:
            self.__listbox.insert(END, item[0])
