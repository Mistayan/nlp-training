import logging

from src.conf import LOGGING_LEVEL


class Logger:

        def __init__(self, name=__name__, level=LOGGING_LEVEL, log_file='log.txt'):
            self.__logger = logging.getLogger(name)
            self.__logger.setLevel(level)
            self.__handler = logging.FileHandler(log_file)
            self.__handler.setLevel(level)
            self.__formatter = logging.Formatter('%(asctime)s - [%(name)s %(lineno)] - %(levelname)s - %(message)s')
            self.__handler.setFormatter(self.__formatter)
            self.__logger.addHandler(self.__handler)

        def info(self, message):
            self.__logger.info(message)

        def error(self, message, error):
            self.__logger.error(message, error)
