import psycopg2
from src.utils import Logger


class Database:

    def __init__(self):
        self.__logger = Logger.Logger()
        self.__connect()
        self.__create_table()

    def __connect(self):
        try:
            self.__connection = psycopg2.connect(user=os.getenv("DB_USER"),
                                                 password=os.getenv("DB_PASS"),
                                                 host=os.getenv("DB_HOST"),
                                                 port=os.getenv("DB_PORT"),
                                                 database=os.getenv("DB_BD")
            self.__cursor = self.__connection.cursor()
            self.__logger.info("Connected to PostgreSQL database")
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while connecting to PostgreSQL database", error)

    def __create_table(self):
        try:
            self.__cursor.execute("CREATE TABLE IF NOT EXISTS email (id SERIAL PRIMARY KEY, body TEXT, category TEXT)")
            self.__connection.commit()
            self.__logger.info("Table created successfully in PostgreSQL database")
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while creating PostgreSQL table", error)

    def insert(self, body, category):
        try:
            self.__cursor.execute("INSERT INTO email (body, category) VALUES (%s, %s)", (body, category))
            self.__connection.commit()
            self.__logger.info("Record inserted successfully into PostgreSQL table")
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while inserting record into PostgreSQL table", error)

    def get_all(self):
        try:
            self.__cursor.execute("SELECT * FROM email")
            self.__logger.info("Selecting rows from PostgreSQL table using cursor.fetchall")
            return self.__cursor.fetchall()
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while selecting rows from PostgreSQL table", error)

    def get(self, id):
        try:
            self.__cursor.execute("SELECT * FROM email WHERE id = %s", (id,))
            self.__logger.info("Selecting rows from PostgreSQL table using cursor.fetchone")
            return self.__cursor.fetchone()
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while selecting rows from PostgreSQL table", error)

    def delete(self, id):
        try:
            self.__cursor.execute("DELETE FROM email WHERE id = %s", (id,))
            self.__connection.commit()
            self.__logger.info("Record deleted successfully from PostgreSQL table")
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while deleting record from PostgreSQL table", error)

    def update(self, id, body, category):
        try:
            sttmt = "UPDATE email SET body = %s, category = %s WHERE id = %s"
            self.__cursor.execute(sttmt, (body, category, id))
            self.__connection.commit()
            self.__logger.info("Record updated successfully in PostgreSQL table")
        except (Exception, psycopg2.Error) as error:
            self.__logger.error("Error while updating record in PostgreSQL table", error)
