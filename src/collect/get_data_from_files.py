import json
from typing import List, Dict, Tuple

import pandas as pd
from tabulate import tabulate

from src.conf import project_file


def get_csv_data(abs_path, show=True) -> List[Tuple[str, Dict[str, Dict[str, int]]]]:
    """ from csv to list of tuples (text, annotations) """
    with open(project_file("cat_token_dict.json", "data"), "r") as fp:
        cat_token_dict = json.load(fp)
    df = pd.read_csv(abs_path, sep=';')

    data = []
    for index, row in df.iterrows():
        data.append((str(row['text']), {'cats': {str(row['category']): 1}}))
    # display each category weight
    if show:
        print("\n"*3, "Samples: ")
        print(tabulate(df, headers='keys', tablefmt='psql'))
        df2 = pd.DataFrame(columns=['category', 'weight'])
        for cat, keywords in cat_token_dict.items():
            df2 = df2._append({'category': cat, 'weight': len(df[df['category'] == cat])}, ignore_index=True)
        print("\n"*3, "Categories: ")
        print(tabulate(df2, headers='keys', tablefmt='psql'))
    return data
