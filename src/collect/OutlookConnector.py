# outlook connector class
from __future__ import print_function

import os

import win32com
import win32com.client
from win32com.client import *
# from win32com.client.dynamic import *


class OutlookConnector:
    def __init__(self, folder_name, path_to_save, shared_mailbox_name):
        self.folder_name = folder_name
        self.path_to_save = path_to_save
        self.outlook: CDispatch = win32com.client.Dispatch("Outlook.Application")
        self.mapi = self.outlook.GetNamespace("MAPI")

        print("available accounts:")
        for account in self.mapi.Accounts:
            print(account.DeliveryStore.DisplayName)
            print(account.DisplayName)
            print(account.UserName)
            print(account.SmtpAddress)
            print(account.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress)

        print("available folders:")
        for folder in self.mapi.Folders:
            print(folder.Name, " : ", folder.EntryID)
        # Accédez à la boîte mail partagée en utilisant le nom de la boîte
        shared_mailbox = self.mapi.Folders(shared_mailbox_name)
        self.inbox = shared_mailbox.Folders.GetFolderFromID(6)  # 6 correspond au dossier de la boîte de réception
        self.subfolder = self.inbox.Folders(folder_name)
        self.messages = self.subfolder.Items

    def get_messages(self):
        return self.messages

    def get_message(self, index):
        return self.messages[index]

    def get_message_body(self, index):
        return self.messages[index].Body

    def get_message_subject(self, index):
        return self.messages[index].Subject

    def get_message_sender(self, index):
        return self.messages[index].SenderName

    def get_message_date(self, index):
        return self.messages[index].SentOn

    def get_message_attachments(self, index):
        return self.messages[index].Attachments

    def get_message_attachments_count(self, index):
        return self.messages[index].Attachments.Count

    def get_message_attachment_name(self, index, attachment_index):
        return self.messages[index].Attachments.Item(attachment_index).FileName

    def get_message_attachment_path(self, index, attachment_index):
        return os.path.join(self.path_to_save, self.messages[index].Attachments.Item(attachment_index).FileName)

    def get_message_attachment_size(self, index, attachment_index):
        return self.messages[index].Attachments.Item(attachment_index).Size

    def get_message_attachment_type(self, index, attachment_index):
        return self.messages[index].Attachments.Item(attachment_index).Type

    def save_attachment(self, index, attachment_index):
        self.messages[index].Attachments.Item(attachment_index).SaveAsFile(
            self.get_message_attachment_path(index, attachment_index))

    def get_message_categories(self, index):
        return self.messages[index].Categories

    # def update_message_categories(self, index, categories):
    #     self.messages[index].Categories = categories
    #     self.messages[index].Save()


if __name__ == '__main__':
    con = OutlookConnector("Inbox", "../../data", "me")
    msg = con.get_message(0)
    print(msg.Subject)
    print(msg.Body)
    print(msg.Sender)
    print(msg.Categories)

