import logging
import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = ROOT_DIR.split("src")[0] + "data"
LOGGING_LEVEL = logging.INFO


def project_file(file_name, dir_name=""):
    if dir_name == "data":
        return os.path.join(DATA_DIR, file_name)
    return os.path.join(ROOT_DIR, file_name)
