import json
import os
import random
from typing import Dict, Set, List, Tuple

import spacy
from spacy.matcher import PhraseMatcher
from spacy.training import Example

from src.collect.get_data_from_files import get_csv_data
from src.conf import project_file

with open(project_file("cat_token_dict.json", "data"), "r") as f:
    cat_token_dict: Dict[str, Set[str]] = json.load(fp=f)


def test(test_text, expected_cat):
    doc = nlp(test_text)
    print(test_text, doc.cats)
    possible = max(doc.cats, key=doc.cats.get)
    print("La catégorie la plus probable est:", possible)
    assert possible == expected_cat


def train_spacy(nlp):
    train_data: List[Tuple[str, Dict[str, Dict[str, int]]]] = get_csv_data(project_file("emails.csv", "data"))

    seed = random.randint(1, 1025)
    print("random seed: ", seed)
    random.seed(seed)  # Pour la reproductibilité
    spacy.util.fix_random_seed(seed)

    optimizer = nlp.begin_training()
    losses = {}
    for i in range(1, 21):
        print("Epoch : ", i)
        random.shuffle(train_data)
        for text, annotations in train_data:
            nlp_tokens = nlp.make_doc(text)
            example = Example.from_dict(nlp_tokens, annotations)
            nlp.update([example], drop=0.25, losses=losses)  #, sgd=optimizer)
        if losses > 75 : break
    return seed, losses, nlp


if __name__ == '__main__':

    nlp = spacy.load("fr_core_news_sm")

    print("init matchers")
    pm = PhraseMatcher(nlp.vocab, attr="LOWER")
    # nlp.add_pipe('ner', last=True)
    # nlp.add_pipe('tagger', last=True)
    nlp.add_pipe('textcat', last=True)
    textcat = nlp.get_pipe("textcat")

    # preparing training data
    for cat, keywords in cat_token_dict.items():
        patterns = [nlp.make_doc(text) for text in keywords]
        textcat.add_label(cat.upper())
        pm.add(key=cat.upper(), docs=patterns, on_match=None)

    seed, losses, model = train_spacy(nlp)
    print("Losses : ", losses)

    # Testez le modèle et affichez les résultats
    # nous ne voudrions pas que le modèle soit éronné
    test_text = "Besoin urgent de recevoir notre paiement, qui était dû le 30/06/2021."
    test(test_text, "RECOUVREMENT")
    test_text = "Bonjour, j'aurais besoin que vous me fassiez votre meilleure offre de prix. Voici le lien https://amazon.fr/xxxx."
    test(test_text, "DEVIS")
    test_text = "Bonjour, votre facture en ligne est disponnible :  https://amazon.fr/invoices/xxxx."
    test(test_text, "AR FACTU")
    # fin tests exemple

    print("valide")
    # Sauvegardez le modèle

    nlp.to_disk(project_file("fr_commercial_model"))
    # save seed in folder
    with open("../fr_commercial_model/seed.txt", "w") as f:
        f.write(str(seed))
