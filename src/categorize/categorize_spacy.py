import os
import sys

import pandas as pd
import spacy
from langdetect import detect

from src.conf import ROOT_DIR, project_file

# Texte à analyser
text = """
besoin urgent de recevoir notre paiement, qui était dû le 30/06/2021.
"""

class Categorizer:

    def __init__(self):
        self.current_lng = None
        self.__nlp = None
        self.__fr_nlp = spacy.load(project_file("fr_commercial_model"))
        self.__en_nlp = spacy.load("en_core_web_sm")
        pass

    def __detect_language(self, text):
        # Détecter la langue du texte
        lng = detect(text)
        print(lng)
        if lng == self.current_lng:
            pass
        elif lng == 'fr' and self.current_lng != 'fr':
            self.__nlp = self.__fr_nlp
            self.current_lng = 'fr'
        elif lng == 'en' and self.current_lng != 'en':
            self.__nlp = self.__en_nlp
            self.current_lng = 'en'
        else:
            raise "Unknown language"

    # Fonction de détection de la catégorie
    def detect(self, text):
        self.__detect_language(text)

        doc = self.__nlp(text)
        print("text\tidx\tlemma\tponctuation\tspace\tshape")
        df = pd.DataFrame(columns=['text', 'idx', 'lemma', 'ponctuation', 'space', 'shape', 'pos', 'tag', 'type'])
        for token in doc:
            df = df._append({'text': token.text, 'idx': token.idx, 'lemma': token.lemma_, 'ponctuation': token.is_punct,
                            'space': token.is_space, 'shape': token.shape_}, ignore_index=True)
        print(df)
        print("text\tidx\tlemma\t\tponctuation\tspace\tshape")
        # categorize the text from doc
        print("textcat: ", doc.cats)
        return max(doc.cats, key=doc.cats.get), self.current_lng


if __name__ == '__main__':
    if (len(sys.argv) > 1):
        text = sys.argv[1]

    # Détecter la catégorie du texte
    categorizer = Categorizer()
    category, language = categorizer.detect(text)
    print(f"Catégorie détectée : {category}, Langue du texte : {language}")
