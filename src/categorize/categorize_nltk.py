"""
Author: Mistayan
Created: 2023/10/20
Last Edited: ${DATE}
Description:
    This file contains the methods to categorize texts
"""
import functools
import os
import re
import sys

import nltk
import pandas as pd
import nltk.data
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer

nltk.download('stopwords')  # Download stopwords : words that are not important
nltk.download('wordnet')  # Download wordnet : a lexical database for the English language
nltk.download('punkt')  # Download punkt : a pre-trained model for English
#french

nltk.download('averaged_perceptron_tagger')  # Download averaged_perceptron_tagger : a pre-trained model for English

# Global variables
n_clusters = 5
n_top_words = 10
plot = False
tokenizer = None
vectorizer = None


def set_tokenizer(lng):

    def __tolist__(data):
        return list(set(data))

    global tokenizer, vectorizer, stopwords
    if lng == 'fr':
        tokenizer = nltk.data.load('tokenizers/punkt/french.pickle')
        stopwords = __tolist__(nltk.corpus.stopwords.words('french'))
    elif lng == 'en':
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        stopwords = __tolist__(nltk.corpus.stopwords.words('english'))
    else:
        print("Language not supported : ", lng)
        exit(0)

    vectorizer = functools.partial(TfidfVectorizer, {'stop_words': list(stopwords), 'tokenizer': tokenizer, 'ngram_range': (1, 2)})


def get_folder_data(folder):
    """
    Get data from the data folder
    :return: a list of data
    """
    data = []
    for filename in os.listdir(folder):
        data.append(extract_data(os.path.join(folder, filename)))
    return data


def doc():
    print("""args:
            file_name (required) [accepts : xml, csv, xlsx, html, json || String]
                - if a file is given, the program will read the file
                - if a folder is given, the program will read all the files in the folder
                - if a url is given, the program will read the html content of the url
                - if "a string is given", the program will read the string, with (")
            --c n_clusters (default to 5)
                - the number of clusters to be created
                - the number of clusters must be less than the number of data
                - the number of clusters must be greater than 0
            --t n_top_words (default to 10)
                - the number of top words to be displayed
                - the number of top words must be greater than 0
            --p (plot the result)
                - plot the result
            -h (print this help message)
            ? (print this help message)
            """)
    exit(0)


def set_opts(param):
    global n_clusters, n_top_words, plot
    if param[0] == '--c':
        n_clusters = int(param[1])
    elif param[0] == '--t':
        n_top_words = int(param[1])
    elif param[0] == '--p':
        plot = True
    else:
        print('Invalid parameter')
        doc()


def extract_data(file_name):
    extension = file_name.split('.')[-1]
    df = None
    if extension == 'xml':
        df = pd.read_xml(file_name)
    elif extension == 'csv':
        df = pd.read_csv(file_name)
    elif extension == 'xlsx':
        df = pd.read_excel(file_name)
    elif extension == 'html':
        df = pd.read_html(file_name)
    elif extension == 'json':
        df = pd.read_json(file_name)
    else:
        print('Invalid file type')
        doc()
    return df


def cluster(vectors):
    global n_clusters
    # vectors is a sparse matrix
    print("Clustering... [n_clusters = {}]".format(n_clusters))
    i, j = vectors.shape
    size = i
    if n_clusters > size:
        n_clusters = size
        print("n_clusters must be less than the number of data : n_clusters = {}".format(n_clusters))
    if n_clusters <= 0:
        n_clusters = 1
        print("n_clusters must be greater than 0... Setting to 1 : n_clusters = {}".format(n_clusters))

    from sklearn.cluster import KMeans
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(vectors)
    return kmeans.labels_


def plot_clusters(vectors, labels):
    from sklearn.decomposition import PCA
    import matplotlib.pyplot as plt
    pca = PCA(n_components=2)
    principal_components = pca.fit_transform(vectors)
    plt.scatter(principal_components[:, 0], principal_components[:, 1], c=labels)
    plt.show()


def collect(param):
    file_name = param[0]
    if len(param) > 2:
        set_opts(param[1:])
    print("INPUT : ", file_name)
    _data = None
    if os.path.isdir(file_name):
        _data = get_folder_data(file_name)
    elif os.path.isfile(file_name):
        _data = extract_data(file_name)
    else:
        _data = [file_name]
    if not _data:
        print('No data found')
        doc()
    return _data


def preprocess_text(text):
    """
    Preprocess the text
    :param text: the text to be preprocessed
    :return: the preprocessed text
    """
    # Remove all the special characters
    processed_text = re.sub(r'\W', ' ', str(text))

    # Converting to Lowercase
    processed_text = processed_text.lower()

    # Remove all single characters
    processed_text = re.sub(r'\s+[a-zA-Z]\s+', ' ', processed_text)

    # Remove single characters from the start
    processed_text = re.sub(r'\^[a-zA-Z]\s+', ' ', processed_text)

    # Substituting multiple spaces with single space
    processed_text = re.sub(r'\s+', ' ', processed_text, flags=re.I)

    # Removing prefixed 'b'
    processed_text = re.sub(r'^b\s+', '', processed_text)

    # Lemmatization
    processed_text = processed_text.split()
    lemmatizer = WordNetLemmatizer()
    processed_text = [lemmatizer.lemmatize(word) for word in processed_text]
    processed_text = ' '.join(processed_text)

    return processed_text


def clean_data(_data):
    """
    Preprocess (clean) the data
    :param _data: the data to be preprocessed
    :return: the preprocessed data
    """
    print("Preprocessing...")
    preprocessed_data = []
    for text in _data:
        preprocessed_data.append(preprocess_text(text))
    return preprocessed_data


def vectorize(preprocessed_data, lng):
    """
    Vectorize the data
    :param preprocessed_data: the data to be vectorized
    :return: the vectorized data
    """
    print("Vectorizing...")
    vectorizer = TfidfVectorizer(stop_words=list(stopwords))
    vectors = vectorizer.fit_transform(preprocessed_data)
    return vectors


def get_top_words_from_cluster(preprocessed_data, labels, n_top_words):
    global n_clusters, vectorizer

    print("Getting top words...")
    Collections = []
    for i in range(n_clusters):
        Collections.append([])
    for i in range(len(preprocessed_data)):
        Collections[labels[i]].append(preprocessed_data[i])
    top_words = []
    for i in range(n_clusters):
        vectors = vectorizer.fit_transform(Collections[i])
        print("vectors : ", vectors)
        feature_names = vectorizer.get_feature_names()
        print("feature_names : ", feature_names)
        top_words.append([feature_names[j] for j in vectors.toarray().sum(axis=0).argsort()[-n_top_words:]])
    return top_words


def process(_data: dict):
    print("Processing...")
    print(_data)
    for entry in _data:
        _id = entry
        value = _data[entry]['text']
        lng = _data[entry]['language']
        print("id : ", _id, " : ", value, " -> ", lng)

        cleaned_data = clean_data(value)

        set_tokenizer(lng)
        vectors = vectorize(cleaned_data, lng)
        labels = cluster(vectors)
        top_words = get_top_words_from_cluster(cleaned_data, labels, n_top_words)
        print('Top words:', ['Cluster {}: {}'.format(i, top_words[i]) for i in range(len(top_words))])
        if plot:
            plot_clusters(vectors, labels)


def define_language(_data) -> dict[int, str]:
    print("Detecting language... : ", _data)
    from langdetect import detect
    ret = {}
    for i, text in enumerate(_data):
        __lng = detect(text)
        print(i, " : ", __lng)
        ret.setdefault(i + 1, {'text': _data, 'language': __lng})
    return ret


if __name__ == '__main__':
    if len(sys.argv) == 1:
        doc()
    elif sys.argv[1] == '-h' or sys.argv[1] == '?' or sys.argv[1] == '--h':
        doc()
    data = []
    if len(sys.argv) >= 2:
        _data = collect(sys.argv[1:])
        print(_data)
        _data: dict[int, str] = define_language(_data)  ## append text_lng to each extracted texts in _data
        process(_data)
